package edu.vt.crest.ais.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.ResearchBroker;

@RestController
public class InvestigatorController {

	@Autowired
	ResearchBroker researchBroker;
	
	@RequestMapping("/research/investigators")
	public List<Investigator> getInvestigators(@RequestParam(value="fromIndex", defaultValue="1") int fromIndex, @RequestParam(value="toIndex", defaultValue="100") int toIndex) {
		try {
			return researchBroker.getInvestigators(fromIndex, toIndex);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping("/research/investigators/{investigatorId}")
	public Investigator getInvestigator(@PathVariable int investigatorId) {
		try {
			return researchBroker.getInvestigator(investigatorId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping(value="/research/investigators/delete/{investigatorId}", method=RequestMethod.DELETE)
	public void deleteInvestigator(@PathVariable int investigatorId) {
		try {
			researchBroker.deleteInvestigator(investigatorId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping(value="/research/investigators/", method=RequestMethod.PUT)
	public void updateInvestigator(@RequestBody Investigator investigator) {
		try {
			researchBroker.updateInvestigator(investigator);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@RequestMapping(value="/research/investigators/", method=RequestMethod.POST)
	public void createInvestigator(@RequestBody Investigator investigator) {
		try {
			researchBroker.createInvestigator(investigator);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@RequestMapping("/research/investigators/count")
	public long getInvestigatorCount() {
		try {
			return researchBroker.getInvestigatorCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
}
