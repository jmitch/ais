package edu.vt.crest.ais.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Award;
import edu.vt.crest.ais.service.api.ResearchBroker;

@RestController
public class AwardController {

	@Autowired
	ResearchBroker researchBroker;
	
	@RequestMapping(value="/research/awards", method=RequestMethod.GET)
	public List<Award> getAwards(@RequestParam(value="fromIndex", defaultValue="1") int fromIndex, @RequestParam(value="toIndex", defaultValue="100") int toIndex) {
		try {
			return researchBroker.getAwards(fromIndex, toIndex);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@RequestMapping(value="/research/awards/delete/{awardId}", method=RequestMethod.DELETE)
	public void deleteAward(@PathVariable int awardId) {
		try {
			researchBroker.deleteAward(awardId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value="/research/awards/", method=RequestMethod.POST)
	public void createAward(@RequestBody Award[] awards) {
		try {
			for (Award award:awards) {
				researchBroker.createAward(award);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@RequestMapping("/research/awards/count")
	public long getAwardCount() {
		try {
			return researchBroker.getAwardCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}	
}

