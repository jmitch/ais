package edu.vt.crest.ais.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.service.api.ResearchBroker;

@RestController
public class GrantController {

	@Autowired
	ResearchBroker researchBroker;
	
	@RequestMapping("/research/grants")
	public List<Grant> getGrants(@RequestParam(value="fromIndex", defaultValue="1") int fromIndex, @RequestParam(value="toIndex", defaultValue="100") int toIndex) {
		try {
			return researchBroker.getGrants(fromIndex, toIndex);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping("/research/grants/{grantId}")
	public Grant getGrant(@PathVariable int grantId) {
		try {
			return researchBroker.getGrant(grantId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping(value="/research/grants/delete/{grantId}", method=RequestMethod.DELETE)
	public void deleteGrant(@PathVariable int grantId) {
		try {
			researchBroker.deleteGrant(grantId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/research/grants/", method=RequestMethod.PUT)
	public void updateGrant(@RequestBody Grant grant) {
		try {
			researchBroker.updateGrant(grant);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/research/grants/", method=RequestMethod.POST)
	public void createGrant(@RequestBody Grant grant) {
		try {
			researchBroker.createGrant(grant);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@RequestMapping("/research/grants/count")
	public long getGrantCount() {
		try {
			return researchBroker.getGrantCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}	
}
