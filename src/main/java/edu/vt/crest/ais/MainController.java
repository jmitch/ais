package edu.vt.crest.ais;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class MainController {

	@RequestMapping(method=RequestMethod.POST)
	public String index(@RequestParam(value="email") String email, @RequestParam(value="password") String password, Model model) {
		if (email != null && password != null) {
			return "redirect:app.html#/info";
		} else {
			model.addAttribute("email", email);
			model.addAttribute("password", password);
			model.addAttribute("status", "Login Failed!");
			return "index";
		}
	}
	
	@RequestMapping(method=RequestMethod.GET) 
	public String index() {
		return "index";
	}
	
	
}
