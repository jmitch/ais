package edu.vt.crest.ais.service.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Award;

public interface AwardService {

	public List<Award> getAwards(int fromIndex, int toIndex) throws SQLException;
	public Award getAward(int awardId) throws SQLException;
	public void createAward(Award award) throws SQLException;
	public long getAwardCount() throws SQLException;
	public void deleteAward(int awardId) throws SQLException;
}
