package edu.vt.crest.ais.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import edu.vt.crest.ais.beans.Award;
import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.DataService;

@Component
@Qualifier("Hibernate")
public class HibernateDataServiceImpl implements DataService{

	private static SessionFactory factory;
	
	public HibernateDataServiceImpl() throws ExceptionInInitializerError, IOException {
		try{
	         factory = new Configuration().configure().buildSessionFactory();
	      }catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
		
		initializeDatabase();
	}
	
	public void initializeDatabase() throws HibernateException, IOException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    
        InputStreamReader in = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("AwardsDb.sql"));
		BufferedReader reader = new BufferedReader(in);
	    
	    try{
	    	tx = session.beginTransaction();
	    	
	    	String line = null;
			String sqlStatement = "";
			while ((line = reader.readLine()) != null) {
				sqlStatement += line;
				
				if (sqlStatement.indexOf(";") >= 0) {
					//Remove semicolon
					sqlStatement = sqlStatement.replace(';', ' ');

					Query query = session.createSQLQuery(sqlStatement);
					query.executeUpdate();
				
					sqlStatement = "";
					
				}
			}
	    	
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) {
	        	tx.rollback();
	        }
	    }finally {
    		session.close(); 
	    }
	
	 
	}
	
	@Override
	public List<Grant> getGrants(int fromIndex, int toIndex) throws SQLException {
		
		Session session = factory.openSession();
	    Transaction tx = null;
	    List<Grant> grants = null;
	    
	    try{
	    	tx = session.beginTransaction();
	    	Query query = session.createQuery("FROM edu.vt.crest.ais.beans.Grant");
	    	query.setFirstResult(fromIndex);
	    	query.setMaxResults(toIndex - fromIndex);
	        grants = query.list(); 
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) {
	        	tx.rollback();
	        }
	        e.printStackTrace(); 
	    }finally {
	         session.close(); 
	    }
	
	    return grants;
	}

	@Override
	public Grant getGrant(int grantId) throws HibernateException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    Grant grant = null;
	    try{
	    	tx = session.beginTransaction();
	        grant = (Grant)session.get(Grant.class, grantId); 
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	    }
	
	    return grant;
	}

	@Override
	public void updateGrant(Grant currGrant) throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = null;
		Grant updatedGrant = null;
	    try{
	    	tx = session.beginTransaction();
	        updatedGrant = (Grant)session.get(Grant.class, currGrant.getId()); 
	        updatedGrant.setAgency(currGrant.getAgency());
	        updatedGrant.setAmount(currGrant.getAmount());
	        updatedGrant.setTitle(currGrant.getTitle());
	        session.update(updatedGrant);
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	    }

	}

	@Override
	public Integer createGrant(Grant grantToCreate) {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer grantId = null;
		try {
			tx = session.beginTransaction();
			Grant grant = new Grant(grantToCreate.getAgency(),
					grantToCreate.getAmount(), grantToCreate.getTitle());
			grantId = (Integer) session.save(grant);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return grantId;
	}
	
	
	@Override
	public void deleteGrant(int grantId) throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Grant grant = (Grant) session.get(Grant.class, grantId);
			session.delete(grant);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
	}

	@Override
	public Long getGrantCount() throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		Long count = null;
		
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(Grant.class);

			// To get total row count.
			cr.setProjection(Projections.rowCount());
			List rowCount = cr.list();
			count = (Long) rowCount.get(0);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}	
		
		return count;
	}

	@Override
	public List<Investigator> getInvestigators(int fromIndex, int toIndex) throws SQLException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    List<Investigator> investigators = null;
	    
	    try{
	    	tx = session.beginTransaction();
	    	Query query = session.createQuery("FROM edu.vt.crest.ais.beans.Investigator");
	    	query.setFirstResult(fromIndex);
	    	query.setMaxResults(toIndex - fromIndex);
	    	investigators = query.list(); 
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) {
	        	tx.rollback();
	        }
	        e.printStackTrace(); 
	    }finally {
	         session.close(); 
	    }
	
	    return investigators;

	}

	@Override
	public Investigator getInvestigator(int investigatorId) throws SQLException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    Investigator investigator = null;
	    try{
	    	tx = session.beginTransaction();
	    	investigator = (Investigator)session.get(Investigator.class, investigatorId); 
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	    }
	
	    return investigator;		
	}

	@Override
	public void updateInvestigator(Investigator currInvestigator) throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		Investigator updatedInvestigator = null;
	    try{
	    	tx = session.beginTransaction();
	    	updatedInvestigator = (Investigator)session.get(Investigator.class, currInvestigator.getId()); 
	    	updatedInvestigator.setLastName(currInvestigator.getLastName());
	    	updatedInvestigator.setFirstName(currInvestigator.getFirstName());
	    	updatedInvestigator.setDepartment(currInvestigator.getDepartment());
	        session.update(updatedInvestigator);
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	    }		
	}

	@Override
	public Integer createInvestigator(Investigator investigatorToCreate) throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer investigatorId = null;
		try {
			tx = session.beginTransaction();
			Investigator investigator = new Investigator(investigatorToCreate.getLastName(), investigatorToCreate.getFirstName(), investigatorToCreate.getDepartment());
			investigatorId = (Integer) session.save(investigator);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return investigatorId;
	}

	@Override
	public void deleteInvestigator(int investigatorId) throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Investigator investigator = (Investigator) session.get(Investigator.class, investigatorId);
			session.delete(investigator);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
	}

	@Override
	public Long getInvestigatorCount() throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		Long count = null;
		
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(Investigator.class);

			// To get total row count.
			cr.setProjection(Projections.rowCount());
			List rowCount = cr.list();
			count = (Long) rowCount.get(0);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}	
		
		return count;
	}

	@Override
	public List<Award> getAwards(int fromIndex, int toIndex) throws SQLException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    List<Award> awards = null;
	    
	    try{
	    	tx = session.beginTransaction();
	    	Criteria cr = session.createCriteria(edu.vt.crest.ais.beans.Award.class);
	    	cr.setFirstResult(fromIndex);
	    	cr.setMaxResults(toIndex - fromIndex);
	    	cr.setFetchMode("investigator", FetchMode.JOIN);
	    	cr.setFetchMode("grant", FetchMode.JOIN);
	    	awards = cr.list();
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) {
	        	tx.rollback();
	        }
	        e.printStackTrace(); 
	    }finally {
	         session.close(); 
	    }
	
	    return awards;		
		
	}

	@Override
	public Award getAward(int awardId) throws HibernateException {
		Session session = factory.openSession();
	    Transaction tx = null;
	    List<Award> awards = null;
	    try{
	    	tx = session.beginTransaction();
	    	
	    	Criteria cr = session.createCriteria(edu.vt.crest.ais.beans.Award.class);
	    	cr.add(Restrictions.eq("id", awardId));
	    	cr.setFetchMode("investigator", FetchMode.JOIN);
	    	cr.setFetchMode("grant", FetchMode.JOIN);
	    	awards = cr.list();
 
	        tx.commit();
	    }catch (HibernateException e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	    }
	
	    return awards.get(0);
	}	
	
	@Override
	public Integer createAward(Award awardToCreate) {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer awardId = null;
		try {
			tx = session.beginTransaction();
			Award award = new Award(awardToCreate.getInvestigator(), awardToCreate.getGrant());
			awardId = (Integer) session.save(award);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return awardId;
	}

	@Override
	public void deleteAward(int awardId) throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Award award = (Award) session.get(Award.class, awardId);
			session.delete(award);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}		
		
	}

	@Override
	public Long getAwardCount() throws SQLException {
		Session session = factory.openSession();
		Transaction tx = null;
		Long count = null;
		
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(Award.class);

			// To get total row count.
			cr.setProjection(Projections.rowCount());
			List rowCount = cr.list();
			count = (Long) rowCount.get(0);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}	
		
		return count;
	}

}
