package edu.vt.crest.ais.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.DataService;
import edu.vt.crest.ais.service.api.InvestigatorService;

@Component
public class InvestigatorServiceImpl implements InvestigatorService {

	@Autowired
	@Qualifier("Hibernate")
	DataService dataService;
	
	public List<Investigator> getInvestigators(int fromIndex, int toIndex) throws SQLException {
		return dataService.getInvestigators(fromIndex, toIndex);
	}
	
	public Investigator getInvestigator(int investigatorId) throws SQLException {
		return dataService.getInvestigator(investigatorId);
	}
	
	public void deleteInvestigator(int investigatorId) throws SQLException {
		dataService.deleteInvestigator(investigatorId);
	}
	
	public void updateInvestigator(Investigator investigator) throws SQLException {
		dataService.updateInvestigator(investigator);
	}
	
	public void createInvestigator(Investigator investigator) throws SQLException {
		dataService.createInvestigator(investigator);
	}
	
	public long getInvestigatorCount() throws SQLException {
		return dataService.getInvestigatorCount();
	}
}
