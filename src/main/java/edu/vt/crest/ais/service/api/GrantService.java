package edu.vt.crest.ais.service.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Investigator;

public interface GrantService {

	public List<Grant> getGrants(int fromIndex, int toIndex) throws SQLException;
	public Grant getGrant(int grantId) throws SQLException;
	public void updateGrant(Grant grant) throws SQLException;
	public void createGrant(Grant grant) throws SQLException;
	public long getGrantCount() throws SQLException;
	public void deleteGrant(int grantId) throws SQLException;

}
