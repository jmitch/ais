package edu.vt.crest.ais.service.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Investigator;

public interface InvestigatorService {

	public List<Investigator> getInvestigators(int fromIndex, int toIndex) throws SQLException;
	public Investigator getInvestigator(int investigatorId) throws SQLException;
	public void updateInvestigator(Investigator investigator) throws SQLException;
	public void createInvestigator(Investigator investigator) throws SQLException;
	public long getInvestigatorCount() throws SQLException;
	public void deleteInvestigator(int investigatorId) throws SQLException;
}
