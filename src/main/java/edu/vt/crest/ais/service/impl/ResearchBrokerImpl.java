package edu.vt.crest.ais.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Award;
import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.GrantService;
import edu.vt.crest.ais.service.api.AwardService;
import edu.vt.crest.ais.service.api.ResearchBroker;
import edu.vt.crest.ais.service.api.InvestigatorService;

@Component
public class ResearchBrokerImpl implements ResearchBroker {

	@Autowired
	GrantService grantService;
	
	@Autowired
	InvestigatorService investigatorService;
	
	@Autowired
	AwardService awardService;
	
	public List<Grant> getGrants(int fromIndex, int toIndex) throws SQLException {
		return grantService.getGrants(fromIndex, toIndex);
	}
	
	public Grant getGrant(int grantId) throws SQLException {
		return grantService.getGrant(grantId);
	}

	public void deleteGrant(int grantId) throws SQLException {
		grantService.deleteGrant(grantId);
	}
	
	public void updateGrant(Grant grant) throws SQLException {
		grantService.updateGrant(grant);
	}
	
	public void createGrant(Grant grant) throws SQLException {
		grantService.createGrant(grant);
	}	
	
	public long getGrantCount() throws SQLException {
		return grantService.getGrantCount();
	}
	
	public List<Investigator> getInvestigators(int fromIndex, int toIndex) throws SQLException {
		return investigatorService.getInvestigators(fromIndex, toIndex);
	}
	
	public Investigator getInvestigator(int investigatorId) throws SQLException {
		return investigatorService.getInvestigator(investigatorId);
	}

	public void deleteInvestigator(int investigatorId) throws SQLException {
		investigatorService.deleteInvestigator(investigatorId);
	}
	
	public void updateInvestigator(Investigator investigator) throws SQLException {
		investigatorService.updateInvestigator(investigator);
	}
	
	public void createInvestigator(Investigator investigator) throws SQLException {
		investigatorService.createInvestigator(investigator);
	}	
	
	public long getInvestigatorCount() throws SQLException {
		return investigatorService.getInvestigatorCount();
	}

	public List<Award> getAwards(int fromIndex, int toIndex) throws SQLException {
		return awardService.getAwards(fromIndex, toIndex);
	}

	public Award getAward(int awardId) throws SQLException {
		return awardService.getAward(awardId);
	}
	
	public void deleteAward(int awardId) throws SQLException {
		awardService.deleteAward(awardId);
	}

	public void createAward(Award award) throws SQLException {
		awardService.createAward(award);
	}

	public long getAwardCount() throws SQLException {
		return awardService.getAwardCount();
	}

	
}
