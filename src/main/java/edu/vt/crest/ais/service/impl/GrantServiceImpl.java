package edu.vt.crest.ais.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.GrantService;
import edu.vt.crest.ais.service.api.DataService;

@Component
public class GrantServiceImpl implements GrantService {

	@Autowired
	@Qualifier("Hibernate")
	DataService dataService;
	
	public List<Grant> getGrants(int fromIndex, int toIndex) throws SQLException {
		return dataService.getGrants(fromIndex, toIndex);
	}
	
	public Grant getGrant(int grantId) throws SQLException {
		return dataService.getGrant(grantId);
	}
	
	public void deleteGrant(int grantId) throws SQLException {
		dataService.deleteGrant(grantId);
	}
	
	public void updateGrant(Grant grant) throws SQLException {
		dataService.updateGrant(grant);
	}
	
	public void createGrant(Grant grant) throws SQLException {
		dataService.createGrant(grant);
	}
	
	public long getGrantCount() throws SQLException {
		return dataService.getGrantCount();
	}

}
