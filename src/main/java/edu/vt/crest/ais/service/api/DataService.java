package edu.vt.crest.ais.service.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Award;
import edu.vt.crest.ais.beans.Investigator;

public interface DataService {

	public List<Grant> getGrants(int fromIndex, int toIndex) throws SQLException;
	public Grant getGrant(int grantId) throws SQLException;
	public void updateGrant(Grant grant) throws SQLException;
	public Integer createGrant(Grant grant) throws SQLException;
	public void deleteGrant(int grantId) throws SQLException;
	public Long getGrantCount() throws SQLException;
	
	public List<Investigator> getInvestigators(int fromIndex, int toIndex) throws SQLException;
	public Investigator getInvestigator(int investigatorId) throws SQLException;
	public void updateInvestigator(Investigator investigator) throws SQLException;
	public Integer createInvestigator(Investigator investigator) throws SQLException;
	public void deleteInvestigator(int investigatorId) throws SQLException;
	public Long getInvestigatorCount() throws SQLException;
	
	public List<Award> getAwards(int fromIndex, int toIndex) throws SQLException;
	public Award getAward(int awardId) throws SQLException;
	public Integer createAward(Award award) throws SQLException;
	public void deleteAward(int awardId) throws SQLException;
	public Long getAwardCount() throws SQLException;	
	

}
