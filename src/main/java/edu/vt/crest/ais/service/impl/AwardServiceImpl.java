package edu.vt.crest.ais.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import edu.vt.crest.ais.beans.Grant;
import edu.vt.crest.ais.beans.Award;
import edu.vt.crest.ais.beans.Investigator;
import edu.vt.crest.ais.service.api.GrantService;
import edu.vt.crest.ais.service.api.DataService;
import edu.vt.crest.ais.service.api.AwardService;

@Component
public class AwardServiceImpl implements AwardService {

	@Autowired
	@Qualifier("Hibernate")
	DataService dataService;
	
	public List<Award> getAwards(int fromIndex, int toIndex) throws SQLException {
		return dataService.getAwards(fromIndex, toIndex);
	}
	
	public void deleteAward(int awardId) throws SQLException {
		dataService.deleteAward(awardId);
	}
	
	public void createAward(Award award) throws SQLException {
		dataService.createAward(award);
	}
	
	public long getAwardCount() throws SQLException {
		return dataService.getAwardCount();
	}

	public Award getAward(int awardId) throws SQLException {
		return dataService.getAward(awardId);
	}

}
