package edu.vt.crest.ais.beans;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Investigator {

	private Integer id;
	private String lastName;
	private String firstName;
	private String department;
	
	public Investigator() {}
	
	public Investigator(String lastName, String firstName, String department) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.department = department;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}	
	
	public String getFullName() {
		return lastName + ", " + firstName;
	}
}
