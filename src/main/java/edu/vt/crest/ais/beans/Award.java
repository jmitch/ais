package edu.vt.crest.ais.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Award {

	private Integer id;
	private Investigator investigator;
	private Grant grant;
	
	public Award() {}
	
	public Award(Investigator investigator, Grant grant) {
		this.investigator = investigator;
		this.grant = grant;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Investigator getInvestigator() {
		return investigator;
	}
	public void setInvestigator(Investigator investigator) {
		this.investigator = investigator;
	}
	public Grant getGrant() {
		return grant;
	}
	public void setGrant(Grant grant) {
		this.grant = grant;
	}
	
	public String getTitle() {
		return investigator.getFullName() + " - " + grant.getTitle();
	}
		
}
