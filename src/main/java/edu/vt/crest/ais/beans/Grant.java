package edu.vt.crest.ais.beans;

import java.math.BigDecimal;

public class Grant {
	
	private Integer id;
	private String title;
	private String agency;
	private BigDecimal amount;
	
	public Grant() {}
	
	public Grant(String agency, BigDecimal amount, String title) {
		this.agency = agency;
		this.amount = amount;
		this.title = title;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}
