angular.module('crest-ais', ['ngRoute'])

.config(function($routeProvider) {
  $routeProvider
    .when('/grants', {
      controller:'GrantController',
      templateUrl:'grant.html'
    }).when('/research/grants/add', {
      controller:'GrantAddController',
      templateUrl:'grant-add.html'
    }).when('/research/grants/edit/:grantId', {
      controller:'GrantEditController',
      templateUrl:'grant-edit.html'
    }).when('/investigators', {
        controller:'InvestigatorController',
        templateUrl:'investigator.html'
    }).when('/research/investigators/add', {
        controller:'InvestigatorAddController',
        templateUrl:'investigator-add.html'
    }).when('/research/investigators/edit/:investigatorId', {
        controller:'InvestigatorEditController',
        templateUrl:'investigator-edit.html'  
    }).when('/awards', {
	    controller:'AwardController',  
	    templateUrl:'award.html'
    }).when('/research/awards/add', {
	    controller:'AwardAddController',
	    templateUrl:'award-add.html'
    }).when('/info', {
    	controller:'InfoController', 
	    templateUrl:'info.html'	    	
    }).otherwise({
    	redirectTo:'/'
    }); 
})

.controller('InfoController', function($scope, $location) {
	$scope.displayGrants = function() {
		$location.path('/grants');
	}
	
	$scope.displayInvestigators = function() {
		$location.path('/investigators');
	}
	
	$scope.displayAwards = function() {
		$location.path('/awards');
	}

})

.controller('AwardAddController', function($scope, $http, $routeParams, $location) {
		
	$scope.addAward = function() {
		
		var awards = [];
		
		for (var j=0; j < $scope.selectedInvestigators.length; j++) {
			var selectedInvestigator = $scope.selectedInvestigators[j];
				
			var award = {'grant': $scope.selectedGrant, 'investigator': selectedInvestigator};
				
			awards.push(award);
		}
		
		$http.post('/research/awards/', angular.toJson(awards)).
		  success(function(data, status, headers, config) {
			  $location.path("/awards");  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}

    $http.get('/research/grants').
	  success(function(data, status, headers, config) {
	    $scope.grants = data;     	  
	  }).
	  error(function(data, status, headers, config) {
	       	  
	  });
	

	$http.get('/research/investigators').
	  success(function(data, status, headers, config) {
      $scope.investigators = data;     	  
    }).
    error(function(data, status, headers, config) {
 	  
    });	
	
	$scope.cancelAddAward = function() {
		$location.path("/awards");
	}
})

.controller('AwardController', function($scope, $http, $location) {
	
	$scope.awardList = {};
	
	$scope.addAward = function() {
		$location.path("/research/awards/add")
	}
	
	$scope.confirmDeleteAward = function(award) {
		$scope.awardToDelete = award;
		$('#myModal').modal('show');
	}

	$scope.deleteAward = function(award) {
		$http.delete('/research/awards/delete/' + $scope.awardToDelete.id).
		  success(function(data, status, headers, config) {
			  $('#myModal').modal('hide');
			  $scope.awardToDelete = undefined;
			  
			  $scope.awardCount--;
			  
			  var batchCount = Math.floor($scope.awardCount / $scope.batchSize);
			  var remainder = $scope.awardCount % $scope.batchSize;
				  
			  if (remainder != 0) {
			    batchCount++;
			  }
				
			  $scope.batchCount = batchCount;
			  
			  $scope.fetchBatch();
			  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}
		
	
	$scope.getAwardCount = function() {
	      $http.get('/research/awards/count').
		    success(function(data, status, headers, config) {
			  var awardCount = data;
			  
			  var batchCount = Math.floor(awardCount / $scope.batchSize);
			  var remainder = awardCount % $scope.batchSize;
				  
			  if (remainder != 0) {
			    batchCount++;
			  }
				
			  $scope.batchCount = batchCount;
			  
			  $scope.awardCount = awardCount;
			}).
			  error(function(data, status, headers, config) {
			  	  
		      });
			}
			
		$scope.fetchBatch = function() {
		  var fromIndex = $scope.batchIndex * $scope.batchSize;
	      var toIndex = fromIndex + $scope.batchSize;
	          
	      if (toIndex > ($scope.awardCount - 1)) {
	        toIndex = $scope.awardCount;
	      }
		    	  
		  $http.get('/research/awards' + '?fromIndex=' + fromIndex + '&toIndex=' + toIndex).
		    success(function(data, status, headers, config) {
		      $scope.awards = data;     	  
		    }).
		      error(function(data, status, headers, config) {
		       	  
		    });
		  }	
		
		$scope.updateBatchCount = function() {
		  var batchCount = Math.floor($scope.awardCount / $scope.batchSize);
		  var remainder = $scope.awardCount % $scope.batchSize;
				  
		  if (remainder != 0) {
		    batchCount++;
		  }
				
		  $scope.batchCount = batchCount;
			
		}
		
		if (!$scope.batchIndex) {
		  $scope.batchIndex = 0;
		}
		
		if (!$scope.batchSize) {
		  $scope.batchSize = 3;	
		}
		
		if (!$scope.awardCount) {
		  $scope.getAwardCount();
		}
		
		$scope.firstBatch = function() {
			$scope.batchIndex = 0;
			$scope.fetchBatch();
		}
		
		$scope.prevBatch = function() {
	      if ($scope.batchIndex <= 0) {
	    	  $scope.batchIndex = ($scope.batchCount - 1);
	      } else {
	    	  $scope.batchIndex--;
	      }
		  $scope.fetchBatch();      
		}
		
		$scope.nextBatch = function() {
			if ($scope.batchIndex >= ($scope.batchCount - 1)) {
				$scope.batchIndex = 0;
			} else {
				$scope.batchIndex++;
			}
			$scope.fetchBatch();
		}
		
		$scope.lastBatch = function() {
		  $scope.batchIndex = $scope.batchCount - 1;
		  $scope.fetchBatch();
		}
	    
	    $scope.fetchBatch();	
	
})
////////////////////////////////////////////////////

.controller('GrantEditController', function($scope, $http, $routeParams, $location) {
	
	$http.get('/research/grants/' + $routeParams.grantId).
	  success(function(data, status, headers, config) {
	    $scope.grant = data;     	  
	  }).
	    error(function(data, status, headers, config) {
	     	  
	  });
	
	$scope.updateGrant = function() {
		
		$http.put('/research/grants/', $scope.grant).
		  success(function(data, status, headers, config) {
			  $location.path("/grants");  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}
	
	$scope.cancelUpdate = function() {
		$location.path("/grants");
	}
})

.controller('GrantAddController', function($scope, $http, $routeParams, $location) {
	
	$scope.grant = {
	  "id": -1,
	  "title": "",
	  "agency": "",
	  "amount": 0
	}
	
	$scope.addGrant = function() {
		
		$http.post('/research/grants/', $scope.grant).
		  success(function(data, status, headers, config) {
			  $scope.grant.id = data;
			  $location.path("/grants");  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}
	
	$scope.cancelAddGrant = function() {
		$location.path("/grants");
	}
})

.controller('GrantController', function($scope, $http, $location) {
	
	$scope.addGrant = function() {
		$location.path("/research/grants/add")
	}
	
	$scope.confirmDeleteGrant = function(grant) {
		$scope.grantToDelete = grant;
		$('#myModal').modal('show');
	}

	$scope.deleteGrant = function(grant) {
		$http.delete('/research/grants/delete/' + $scope.grantToDelete.id).
		  success(function(data, status, headers, config) {
			  $('#myModal').modal('hide');
			  $scope.grantToDelete = undefined;
			  
			  $scope.grantCount--;
			  
			  var batchCount = Math.floor($scope.grantCount / $scope.batchSize);
			  var remainder = $scope.grantCount % $scope.batchSize;
				  
			  if (remainder != 0) {
			    batchCount++;
			  }
				
			  $scope.batchCount = batchCount;
			  
			  $scope.fetchBatch();
			  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });		
	}
	
	$scope.getGrantCount = function() {
      $http.get('/research/grants/count').
	    success(function(data, status, headers, config) {
		  var grantCount = data;
		  
		  var batchCount = Math.floor(grantCount / $scope.batchSize);
		  var remainder = grantCount % $scope.batchSize;
			  
		  if (remainder != 0) {
		    batchCount++;
		  }
			
		  $scope.batchCount = batchCount;
		  
		  $scope.grantCount = grantCount;
		}).
		  error(function(data, status, headers, config) {
		  	  
	      });
		}
		
	$scope.fetchBatch = function() {
	  var fromIndex = $scope.batchIndex * $scope.batchSize;
      var toIndex = fromIndex + $scope.batchSize;
          
      if (toIndex > ($scope.grantCount - 1)) {
        toIndex = $scope.grantCount;
      }
	    	  
	  $http.get('/research/grants' + '?fromIndex=' + fromIndex + '&toIndex=' + toIndex).
	    success(function(data, status, headers, config) {
	      $scope.grants = data;     	  
	    }).
	      error(function(data, status, headers, config) {
	       	  
	    });
	  }	
	
	$scope.updateBatchCount = function() {
	  var batchCount = Math.floor($scope.grantCount / $scope.batchSize);
	  var remainder = $scope.grantCount % $scope.batchSize;
			  
	  if (remainder != 0) {
	    batchCount++;
	  }
			
	  $scope.batchCount = batchCount;
		
	}
	
	if (!$scope.batchIndex) {
	  $scope.batchIndex = 0;
	}
	
	if (!$scope.batchSize) {
	  $scope.batchSize = 3;	
	}
	
	if (!$scope.grantCount) {
	  $scope.getGrantCount();
	}
	
	$scope.firstBatch = function() {
		$scope.batchIndex = 0;
		$scope.fetchBatch();
	}
	
	$scope.prevBatch = function() {
      if ($scope.batchIndex <= 0) {
    	  $scope.batchIndex = ($scope.batchCount - 1);
      } else {
    	  $scope.batchIndex--;
      }
	  $scope.fetchBatch();      
	}
	
	$scope.nextBatch = function() {
		if ($scope.batchIndex >= ($scope.batchCount - 1)) {
			$scope.batchIndex = 0;
		} else {
			$scope.batchIndex++;
		}
		$scope.fetchBatch();
	}
	
	$scope.lastBatch = function() {
	  $scope.batchIndex = $scope.batchCount - 1;
	  $scope.fetchBatch();
	}
    
    $scope.fetchBatch();
})
//////////////////////////////////////////////////
.controller('InvestigatorController', function($scope, $http, $location) {
	
	$scope.addInvestigator = function() {
		$location.path("/research/investigators/add")
	}
	
	$scope.confirmDeleteInvestigator = function(investigator) {
		$scope.investigatorToDelete = investigator;
		$('#myModal').modal('show');
	}

	$scope.deleteInvestigator = function(investigator) {
		$http.delete('/research/investigators/delete/' + $scope.investigatorToDelete.id).
		  success(function(data, status, headers, config) {
			  $('#myModal').modal('hide');
			  $scope.investigatorToDelete = undefined;
			  
			  $scope.investigatorCount--;
			  
			  var batchCount = Math.floor($scope.investigatorCount / $scope.batchSize);
			  var remainder = $scope.investigatorCount % $scope.batchSize;
				  
			  if (remainder != 0) {
			    batchCount++;
			  }
				
			  $scope.batchCount = batchCount;
			  
			  $scope.fetchBatch();
			  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });		
	}
	
	$scope.getInvestigatorCount = function() {
      $http.get('/research/investigators/count').
	    success(function(data, status, headers, config) {
		  var investigatorCount = data;
		  
		  var batchCount = Math.floor(investigatorCount / $scope.batchSize);
		  var remainder = investigatorCount % $scope.batchSize;
			  
		  if (remainder != 0) {
		    batchCount++;
		  }
			
		  $scope.batchCount = batchCount;
		  
		  $scope.investigatorCount = investigatorCount;
		}).
		  error(function(data, status, headers, config) {
		  	  
	      });
		}
		
	$scope.fetchBatch = function() {
	  var fromIndex = $scope.batchIndex * $scope.batchSize;
      var toIndex = fromIndex + $scope.batchSize;
          
      if (toIndex > ($scope.investigatorCount - 1)) {
        toIndex = $scope.investigatorCount;
      }
	    	  
	  $http.get('/research/investigators' + '?fromIndex=' + fromIndex + '&toIndex=' + toIndex).
	    success(function(data, status, headers, config) {
	      $scope.investigators = data;     	  
	    }).
	      error(function(data, status, headers, config) {
	       	  
	    });
	  }	
	
	$scope.updateBatchCount = function() {
	  var batchCount = Math.floor($scope.investigatorCount / $scope.batchSize);
	  var remainder = $scope.investigatorCount % $scope.batchSize;
			  
	  if (remainder != 0) {
	    batchCount++;
	  }
			
	  $scope.batchCount = batchCount;
		
	}
	
	$scope.editInvestigator = function() {
		
	}
	
	if (!$scope.batchIndex) {
	  $scope.batchIndex = 0;
	}
	
	if (!$scope.batchSize) {
	  $scope.batchSize = 3;	
	}
	
	if (!$scope.investigatorCount) {
	  $scope.getInvestigatorCount();
	}
	
	$scope.firstBatch = function() {
		$scope.batchIndex = 0;
		$scope.fetchBatch();
	}
	
	$scope.prevBatch = function() {
      if ($scope.batchIndex <= 0) {
    	  $scope.batchIndex = ($scope.batchCount - 1);
      } else {
    	  $scope.batchIndex--;
      }
	  $scope.fetchBatch();      
	}
	
	$scope.nextBatch = function() {
		if ($scope.batchIndex >= ($scope.batchCount - 1)) {
			$scope.batchIndex = 0;
		} else {
			$scope.batchIndex++;
		}
		$scope.fetchBatch();
	}
	
	$scope.lastBatch = function() {
	  $scope.batchIndex = $scope.batchCount - 1;
	  $scope.fetchBatch();
	}
    
    $scope.fetchBatch();
})
////////////////////////////////////////
.controller('InvestigatorEditController', function($scope, $http, $routeParams, $location) {
	
	$http.get('/research/investigators/' + $routeParams.investigatorId).
	  success(function(data, status, headers, config) {
	    $scope.investigator = data;     	  
	  }).
	    error(function(data, status, headers, config) {
	     	  
	  });
	
	$scope.updateInvestigator = function() {
		
		$http.put('/research/investigators/', $scope.investigator).
		  success(function(data, status, headers, config) {
			  $location.path("/investigators");  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}
	
	$scope.cancelUpdate = function() {
		$location.path("/investigators");
	}
})
/////////////////////////////////////////////
.controller('InvestigatorAddController', function($scope, $http, $routeParams, $location) {
	
	$scope.investigator = {
	  "id": -1,
	  "firstName": "",
	  "lastName": "",
	  "department": ""
	}
	
	$scope.addInvestigator = function() {
		
		$http.post('/research/investigators/', $scope.investigator).
		  success(function(data, status, headers, config) {
			  $location.path("/investigators");  
		  }).
		  error(function(data, status, headers, config) {
			  
		  });
	}
	
	$scope.cancelAddInvestigator = function() {
		$location.path("/investigators");
	}
})
