insert into VTGrant(title, agency, amount) values ('Grant 1', 'USDA', 30000);
insert into VTGrant(title, agency, amount) values ('Grant 2', 'USDA', 150000);
insert into VTGrant(title, agency, amount) values ('Grant 3', 'NSF', 200000);
insert into VTGrant(title, agency, amount) values ('Grant 4', 'NSF', 40000);
insert into VTGrant(title, agency, amount) values ('Grant 5', 'NSF', 35000);
insert into VTGrant(title, agency, amount) values ('Grant 6', 'USDA', 25000);
insert into VTGrant(title, agency, amount) values ('Grant 7', 'Microsoft', 10000);
insert into VTGrant(title, agency, amount) values ('Grant 8', 'IBM', 25000);
insert into VTGrant(title, agency, amount) values ('Grant 9', 'Intrexon', 30000);
insert into VTGrant(title, agency, amount) values ('Grant 10', 'Intrexon', 45000);

insert into investigator(last_name, first_name, department) values ('Doe', 'John', 'Electrical Engineering');
insert into investigator(last_name, first_name, department) values ('Doe', 'Jane', 'Crop and Soil Environmental Sciences');
insert into investigator(last_name, first_name, department) values ('Kent', 'Clark', 'Physics');
insert into investigator(last_name, first_name, department) values ('Wayne', 'Bruce', 'Chemistry');
insert into investigator(last_name, first_name, department) values ('Parker', 'Peter', 'Computer Science');
insert into investigator(last_name, first_name, department) values ('Washington', 'George', 'Biology');
insert into investigator(last_name, first_name, department) values ('Jefferson', 'Thomas', 'Bioinformatics');
insert into investigator(last_name, first_name, department) values ('Adams', 'John', 'Statistics');
insert into investigator(last_name, first_name, department) values ('Madison', 'James', 'Mechanical Engineering');
insert into investigator(last_name, first_name, department) values ('Lincoln', 'Abraham', 'Biological Systems Engineering');

insert into award(grant_id, investigator_id) values (1, 1);
insert into award(grant_id, investigator_id) values (1, 2);
insert into award(grant_id, investigator_id) values (1, 3);
insert into award(grant_id, investigator_id) values (2, 4);
insert into award(grant_id, investigator_id) values (2, 5);
insert into award(grant_id, investigator_id) values (2, 6);
insert into award(grant_id, investigator_id) values (2, 7);
insert into award(grant_id, investigator_id) values (3, 1);
insert into award(grant_id, investigator_id) values (3, 2);
insert into award(grant_id, investigator_id) values (3, 3);
insert into award(grant_id, investigator_id) values (3, 4);




